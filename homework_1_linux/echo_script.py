#!/usr/bin/env python

import socket
 
HOST = '92.223.25.87'       # Hostname to bind
PORT = 5000              # Open non-privileged port 5000 
  
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
conn, addr = s.accept()
print 'Connected by', addr
while 1:
    data = conn.recv(1024)
    if not data: break
    conn.send(data)
conn.close()
