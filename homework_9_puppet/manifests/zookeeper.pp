include zookeeper

class zookeeper (
  $version              = 'present',
  $package_name         = 'zookeeper',
  $port                 = '2181',
  $username             = 'zookeeper',
  $template_path        = 'puppet:///modules/zookeeper/zookeeper.erb',
  $init_template_path   = 'puppet:///modules/zookeeper/system.zk.service.erb',
) {

  $config = {
    'maxClientCnxns'            => 0,
    'tickTime'                  => 2000,
    'initLimit'                 => 10,
    'syncLimit'                 => 5,
    'dataDir'                   => '/var/lib/zookeeper',
    'clientPort'                => $port,
    'autopurge.snapRetainCount' => 5,
    'autopurge.purgeInterval'   => 24
  }

  user { $username:
    name => $username
  }

  yumrepo { 'yr':
    ensure => present,
    baseurl => 'http://repos.bigtop.apache.org/releases/1.2.1/centos/7/x86_64',
    gpgkey => 'https://www-us.apache.org/dist/bigtop/bigtop-1.2.1/repos/GPG-KEY-bigtop',
    gpgcheck => 1,
    enabled => 1,
    name => 'apache_Bigtop'
 } ->
    package { $package_name:
    ensure => installed,
 }
    package { 'java-1.8.0-openjdk':
    ensure => installed,
    before => Package[$package_name]
 }

  file { '/etc/zookeeper/conf/zoo.cfg':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => $config,
    require => Package['zookeeper'],
  }

  file { '/usr/lib/systemd/system/zookeeper.service':
    ensure  => file,
    owner   => $username,
    group   => $username,
    content => template($init_template_path),
    notify  => Exex['systemctl-daemon-reload']
    require => [Package[$package_name],User[$username]],
    before  => Service['zookeeper'],
  }

  exec { 'systemctl-daemon-reload':
    refreshonly => true,
    command     => '/usr/bin/systemctl daemon-reload',
  }

  service { 'zookeeper':
    ensure  => 'running',
    enable  => true,
    require => [Package['zookeeper'], File['/etc/zookeeper/conf/zoo.cfg'], File[$config['dataDir']]],
  }

  file { [$config['dataDir'], "${config['dataDir']}/version-2"] :
    ensure       => directory,
    owner        => $username,
    group        => $username,
    mode         => '0755',
    recurse      => true,
    recurselimit => 0,
    require      => Package['zookeeper'],
  }
}
